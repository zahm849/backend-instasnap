package tg.backend.instasnap.configs;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

@Slf4j
public class JsonUtility {

    private JsonUtility() {
    }

    private static ObjectMapper mapper;

    public static ObjectMapper getMapper() {

        if (mapper == null) {
            mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        }
        return mapper;
    }

    public static String serialize(Object object) {
        String result = null;

        try {
            result = JsonUtility.getMapper().writeValueAsString(object);
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }

        return result;
    }

    public static <T> T deserialize(String obj, Class<T> _class) {
        T result = null;

        try {
            result = getMapper().readValue(obj, _class);
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }

        return result;
    }
}
