package tg.backend.instasnap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;
import tg.backend.instasnap.models.Publication;
import tg.backend.instasnap.models.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface PublicationRepository extends JpaRepository<Publication, Long> {
    List<Publication> getByUser(User user);
    //Optional<Publication> findByIdAndDeletedAtNull(Long id);
}
