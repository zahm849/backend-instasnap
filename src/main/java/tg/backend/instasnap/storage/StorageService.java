package tg.backend.instasnap.storage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Service
public class StorageService {

	private final Path rootLocation;
	private final Logger logger = LoggerFactory.getLogger(StorageService.class);

	public StorageService(StorageProperties properties) {

		this.rootLocation = Paths.get(properties.getLocation());
		this.init();
	}

	public String store(MultipartFile file) {
		String originalFileName = file.getOriginalFilename();
		try {
			if (file.isEmpty()) {
				throw new StorageException(
						"Failed to store empty file " + originalFileName);
			}
			String newFileName = System.currentTimeMillis() + "_"
					+ originalFileName.replace(" ", "_").toLowerCase();
			Files.copy(file.getInputStream(),
					this.rootLocation.resolve(newFileName));
			return newFileName;
		} catch (IOException e) {
			throw new StorageException(
					"Failed to store file " + originalFileName, e);
		}
	}

	public Stream<Path> loadAll() {
		try {
			return Files.walk(this.rootLocation, 1)
					.filter(path -> !path.equals(this.rootLocation))
					.map(this.rootLocation::relativize);
		} catch (IOException e) {
			throw new StorageException("Failed to read stored files", e);
		}

	}

	public Path load(String filename) {
		return rootLocation.resolve(filename);
	}

	public Resource loadAsResource(String filename) {
		try {
			Path file = load(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new StorageFileNotFoundException(
						"Could not read file: " + filename);

			}
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException(
					"Could not read file: " + filename, e);
		}
	}

	public void deleteAll() {
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}

	public void init() {

		File directory = new File(rootLocation.toString());
		if (!directory.exists()) {
			directory.mkdir();
		}

		logger.info("Storage initialized");

	}
}
