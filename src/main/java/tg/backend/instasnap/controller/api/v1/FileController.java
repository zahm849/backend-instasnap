package tg.backend.instasnap.controller.api.v1;

import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import lombok.RequiredArgsConstructor;
import tg.backend.instasnap.storage.StorageService;

@RestController
@RequiredArgsConstructor
@CrossOrigin(value = "*",origins = "*",allowedHeaders = "*")
@RequestMapping(value = "api/v1/files")
public class FileController {

    private final StorageService storageService;

    @GetMapping("{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = this.storageService.loadAsResource(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

}