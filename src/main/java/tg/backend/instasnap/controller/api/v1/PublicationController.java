package tg.backend.instasnap.controller.api.v1;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;
import tg.backend.instasnap.configs.JsonUtility;
import tg.backend.instasnap.dto.PublicationDto;
import tg.backend.instasnap.dto.request.PublicationSaveDto;
import tg.backend.instasnap.dto.response.PublicationGetAllResponseDto;
import tg.backend.instasnap.models.Publication;
import tg.backend.instasnap.service.LikerServiceInterface;
import tg.backend.instasnap.service.PublicationServiceInterface;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tg.backend.instasnap.storage.StorageService;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(value = "*",origins = "*",allowedHeaders = "*")
@RequestMapping(path = "api/v1/publications")
@Slf4j
public class PublicationController {
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PublicationServiceInterface publicationServiceInterface;

    private final Logger logger = LoggerFactory
            .getLogger(PublicationController.class);
    private final StorageService storageService;

    @Autowired
    private LikerServiceInterface likerServiceInterface;

    public PublicationController(StorageService storageService) {
        this.storageService = storageService;
    }

    @PostMapping(value = "{id}/file")
    public ResponseEntity<Publication> uploadFile(
            @RequestParam("file") MultipartFile file, @PathVariable Long id) {
        try {
            String image = storageService.store(file);
            Publication updatedPublication = this.publicationServiceInterface.updateImage(id,
                    image);
            return ResponseEntity.ok(updatedPublication);
        } catch (NotFoundException e) {
            logger.error(e.getMessage(), e);

            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .build();
        }
    }

    @GetMapping(value = "/all")
    public ResponseEntity<PublicationGetAllResponseDto> listPublications() {
        TypeToken<PublicationDto> typeToken = new
                TypeToken<PublicationDto>(PublicationDto.class) {
                };
        List<Publication> data = publicationServiceInterface.getAll();
        List<PublicationDto> responseData = new ArrayList<>();
        data.forEach((c) -> {
            PublicationDto dto = modelMapper.map(c, typeToken.getType());
            responseData.add(dto);
        });
        PublicationGetAllResponseDto response = new PublicationGetAllResponseDto(responseData);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/getAllByUser/{id}")
    public ResponseEntity<PublicationGetAllResponseDto> listPublicationsByUser(@PathVariable("id") Long id) {
        TypeToken<PublicationDto> typeToken = new
                TypeToken<PublicationDto>(PublicationDto.class) {
                };
        List<Publication> data = publicationServiceInterface.getAllByUser(id);
        List<PublicationDto> responseData = new ArrayList<>();
        data.forEach((c) -> {
            PublicationDto dto = modelMapper.map(c, typeToken.getType());
            responseData.add(dto);
        });
        PublicationGetAllResponseDto response = new PublicationGetAllResponseDto(responseData);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/new")
    public ResponseEntity<PublicationSaveDto> CreatePublication(
            @RequestBody PublicationSaveDto publicationSaveDto,@RequestPart("file[]") MultipartFile[] files) {
            //String image = storageService.store(file);
        Publication type = this.publicationServiceInterface.create(publicationSaveDto);
        /*Publication updatedPublication = this.publicationServiceInterface.updateImage(publicationSaveDto.getPublication().getId(),
                image);*/
        TypeToken<PublicationDto> typeToken = new
                TypeToken<PublicationDto>(PublicationDto.class) {
                };
        PublicationDto response = modelMapper.map(type, typeToken.getType());


        return new ResponseEntity(response, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Object> add(@RequestParam(name = "file", required = true) MultipartFile file,
                                      @RequestParam("publication") String publicationStr) {
        try {
            PublicationSaveDto publication =  JsonUtility.deserialize(publicationStr, PublicationSaveDto.class);
            if (publication == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }
            String imagePath = storageService.store(file);
            publication.getPublication().setUrl(imagePath);

            publicationServiceInterface.create(publication);
            //publicationService.add(publication, Optional.ofNullable(files));
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @GetMapping(value = "/getPublicationById/{id}")
    @ResponseBody
    public ResponseEntity<PublicationDto> getPublicationById(@PathVariable("id") Long id) {
        TypeToken<PublicationDto> typeToken = new
                TypeToken<PublicationDto>(PublicationDto.class) {
                };
        Publication publication = publicationServiceInterface.getById(id);
        PublicationDto response = modelMapper.map(publication, typeToken.getType());
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<PublicationSaveDto> updatePublication(
            @PathVariable(value = "id") Long id,
            @RequestBody PublicationSaveDto publicationSaveDto) {
        Publication publicationUpdated = publicationServiceInterface.update(publicationSaveDto, id);
        if (publicationUpdated == null) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
        TypeToken<PublicationDto> typeToken = new
                TypeToken<PublicationDto>(PublicationDto.class) {
                };
        PublicationDto response = modelMapper.map(publicationUpdated, typeToken.getType());
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public void deletePublication(@PathVariable Long id) {
        publicationServiceInterface.delete(id);
        System.out.println("Le publication " + id + " a été supprimé avec succès");
    }

    @DeleteMapping("/deleteAuto")
    public void AutoDeletePublication() {
        //publicationServiceInterface.delete(id);
        System.out.println("Le publication " + 00 + " a été supprimé avec succès");
    }

}
